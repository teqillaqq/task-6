//
//  StartViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import Foundation
import MapKit

class NetworkManager {

    static let shared: NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()

    enum HTTPMethod: String {
        case get = "GET"
    }

    func fetchAtms(completionHandler: @escaping (Result<BelarusbankDataATM, Error>) -> Void) {
        guard let url = URL(string: URLs.atmURL) else { return }
        fetch(url: url, completionHandler: completionHandler)
    }

    func fetchInfobox(completionHandler: @escaping (Result<belarusbankDataInfobox, Error>) -> Void) {
        guard let url = URL(string: URLs.infoboxURL) else { return }
        fetch(url: url, completionHandler: completionHandler)
    }

    func fetchBank(completionHandler: @escaping (Result<Bank, Error>) -> Void) {
        guard let url = URL(string: URLs.bankURL) else { return }
        fetch(url: url, completionHandler: completionHandler)
    }

    private func fetch<T:Decodable>(url: URL,
                                    completionHandler: @escaping (Result<T, Error>) -> Void) {
        let request = makeRequest(url: url, method: .get)

        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }

            if let data = data {
                if let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200...299:
                        do {
                            let result: T = try JSONDecoder().decode(T.self, from: data)
                            completionHandler(.success(result))
                        } catch let error {
                            completionHandler(.failure(error))
                        }
                    default:
                        guard let errorDescription = String(data: data, encoding: .utf8) else { return }
                        let error = NSError(domain: errorDescription, code: response.statusCode, userInfo: nil)
                        completionHandler(.failure(error))
                    }
                }
            }
        }.resume()
    }

    private func makeRequest(url: URL, method: HTTPMethod) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue

        return request
    }

    private init() { }
}
