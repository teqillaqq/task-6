//
//  ContentConstants.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import Foundation

enum ContentConstants {
    static let segmentedControlFirstItem = "Карта"
    static let segmentedControlSecondItem = "Список"
    static let reload = "Обновить"
    static let filter = "Фильтр"
    static let description = "Подробнее"
    static let startVCtitle = "Банкоматы"
    static let error = "Ошибка"
    static let failedToLoad = "Не удалось загрузить "
    static let pressTheTefreshButton = "Нажмите кнопку обновить"
    static let close = "Закрыть"
    static let openPhoneSettings = """
    Для продолжения работы приложению требуется
    доступ к геолокации. Хотите открыть настройки и разрешить геолокацию?
    """
    static let change = "Отменв"
    static let openSettings = "Открыть настройки"
    static let atmTitle = "Банкоматы"
    static let infoboxTitle = "Инфокиоски"
    static let bankTitle = "Отделения банков"
    static let errorSwitchVontroller = """
    Error: switchViewController index out of range
    """
    static let showRouteTitle = "Построить маршрут"






}
