//
//  CoordinateConstants.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 6.03.22.
//

import Foundation
import CoreLocation

enum CoordinateConstants {
    static let defaultСoordinates: CLLocation = CLLocation(latitude: 52.425163,
                                                               longitude: 31.015039)
}
