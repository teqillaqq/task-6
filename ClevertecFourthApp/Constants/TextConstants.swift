//
//  TextConstants.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import Foundation
import UIKit

enum TextConstants {
    static let fontSizeOfCollectionView: CGFloat = 14
    static let fontSizePopup: CGFloat = 25
    static let fontSizefullAtmInfo: CGFloat = 22
}
