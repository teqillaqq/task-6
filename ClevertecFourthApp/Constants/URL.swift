//
//  URL.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 27.02.22.
//

import Foundation

enum URLs {
    static let atmURL: String = "https://belarusbank.by/open-banking/v1.0/atms"
    static let infoboxURL: String = "https://belarusbank.by/api/infobox"
    static let bankURL: String = "https://belarusbank.by/open-banking/v1.0/branches"
}
