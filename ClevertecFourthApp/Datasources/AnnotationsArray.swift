

import MapKit
import Contacts

class AnnotationsArray: NSObject, MKAnnotation {

    let type: String
    let idNumber: String
    let city: String
    let title: String?
    let locationName: String?
    let address: String
    let workingHours: String
    let issuedCurrency: String
    let cashIn: String
    let coordinate: CLLocationCoordinate2D
    var subtitle: String? {
        return locationName
    }

    init(type: String, title: String, locationName: String?,
         coordinate: CLLocationCoordinate2D, idNumber: String,
         address: String, workingHours: String, issuedCurrency: String,
         cashIn: String, city: String) {
        self.type = type
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        self.idNumber = idNumber
        self.address = address
        self.workingHours = workingHours
        self.issuedCurrency = issuedCurrency
        self.cashIn = cashIn
        self.city = city
        super.init()
    }

    class func from(atmData: ATM) -> AnnotationsArray? {
        let type = atmData.type.rawValue
        let title = atmData.address.addressLine
        let locationName = atmData.atmID
        let lat = Double(atmData.address.geolocation.geographicCoordinates.latitude) ?? 53.5439
        let long = Double(atmData.address.geolocation.geographicCoordinates.longitude) ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let idNumber = atmData.atmID
        let address = ATM.getAtmAddress(atm: atmData)
        let workingHours = ATM.getAtmworkingHours(atm: atmData)
        let issuedCurrency = ATM.getIssuedCurrency(atm: atmData)
        let cashIn = ATM.getCashIn(atm: atmData)
        let city = ATM.getCity(atm: atmData)

        return AnnotationsArray(type: type, title: title, locationName: locationName,
                                coordinate: coordinate, idNumber: idNumber, address: address,
                                workingHours: workingHours, issuedCurrency: issuedCurrency,
                                cashIn: cashIn, city: city)
    }

    class func from(infoboxData: BelarusbankDataInfobox) -> AnnotationsArray? {
        let type = infoboxData.type
        let title = infoboxData.locationNameDesc
        let locationName = String(infoboxData.infoID)
        let lat = Double(infoboxData.gpsX) ?? 53.5439
        let long = Double(infoboxData.gpsY) ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let idNumber = String(infoboxData.infoID)
        let address = BelarusbankDataInfobox.getAddressgetCity(infobox: infoboxData)
        let workingHours = BelarusbankDataInfobox.getworkingHours(infobox: infoboxData)
        let issuedCurrency = infoboxData.currency.rawValue
        let cashIn = BelarusbankDataInfobox.getCashIn(infobox: infoboxData)
        let city = BelarusbankDataInfobox.getCity(infobox: infoboxData)

        return AnnotationsArray(type: type, title: title, locationName: locationName,
                                coordinate: coordinate, idNumber: idNumber, address: address,
                                workingHours: workingHours, issuedCurrency: issuedCurrency,
                                cashIn: cashIn, city: city)
    }

    class func from(bankData: Branch) -> AnnotationsArray? {
        let type = bankData.type
        let title = bankData.name
        let locationName = bankData.branchID
        let lat = Double(bankData.address.geoLocation.geographicCoordinates.latitude) ?? 53.5439
        let long = Double(bankData.address.geoLocation.geographicCoordinates.longitude) ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        let idNumber = bankData.branchID
        let address = Branch.getAddress(bankData: bankData)
        let workingHours = Branch.getworkingHoursBank(bankData: bankData)
        let issuedCurrency = Branch.getIssuedCurrency(bankData: bankData)
        let cashIn = Branch.getCashIn(bankData: bankData)
        let city = bankData.address.townName
        return AnnotationsArray(type: type, title: title, locationName: locationName,
                                coordinate: coordinate, idNumber: idNumber, address: address,
                                workingHours: workingHours, issuedCurrency: issuedCurrency,
                                cashIn: cashIn, city: city)
    }

    func mapItem() -> MKMapItem {
        let addressDictionary = [(String(CNPostalAddressStreetKey)) : subtitle]
        let placemark = MKPlacemark(coordinate: coordinate,
                                    addressDictionary: addressDictionary as [String : Any])
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(String(describing: title)) \(String(describing: subtitle))"
        return mapItem
    }

    var markerTintColor: UIColor  {
      switch type {
      case "Infobox":
        return .red
      case "ATM":
        return .orange
      case "Bank":
        return .blue
      default:
        return .green
      }
    }

    var image: UIImage {

      switch type {
      case "ATM":
        return #imageLiteral(resourceName: "atm")
      case "Infobox":
          return #imageLiteral(resourceName: "infobox")
      case "Bank":
          return #imageLiteral(resourceName: "bank")
      default:
        return #imageLiteral(resourceName: "savings")
      }
    }
}

