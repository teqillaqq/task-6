//
//  BelarusbankDataBank.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 3.03.22.
//

import Foundation

// MARK: - Bank
struct Bank: Codable {
    let data: DataClassBank
    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }
}

// MARK: - DataClass
struct DataClassBank: Codable {
    let branch: [Branch]

    enum CodingKeys: String, CodingKey {
        case branch = "Branch"
    }
}

// MARK: - Branch
struct Branch: Codable {
    let type = "Bank"
    let branchID, name: String
    let cbu: String?
    let accountNumber: AccountNumber?
    let equeue, wifi: Int
    let address: AddressBank
    let information: Information
    var services: ServicesBank

    enum CodingKeys: String, CodingKey {
        case branchID = "branchId"
        case name
        case cbu = "CBU"
        case accountNumber, equeue, wifi
        case address = "Address"
        case information = "Information"
        case services = "Services"
    }
}

enum AccountNumber: String, Codable {
    case by12Akbb38193821000310000000 = "BY12AKBB 3819 3821 0003 1000 0000"
    case by27Akbb38193821000300000000 = "BY27AKBB 3819 3821 0003 0000 0000"
    case by28Akbb38193821000170000000 = "BY28AKBB 3819 3821 0001 7000 0000"
    case by42Akbb38193821000290000000 = "BY42AKBB 3819 3821 0002 9000 0000"
    case by57Akbb38193821000280000000 = "BY57AKBB 3819 3821 0002 8000 0000"
    case by88Akbb38193821000130000000 = "BY88AKBB 3819 3821 0001 3000 0000"
    case by94Akbb38193821000320000000 = "BY94AKBB 3819 3821 0003 2000 0000"
}

// MARK: - Address
struct AddressBank: Codable {
    let streetName, buildingNumber, department, postCode: String
    let townName, countrySubDivision, country, addressLine: String
    let addressDescription: String
    let geoLocation: GeoLocationBank

    enum CodingKeys: String, CodingKey {
        case streetName, buildingNumber, department, postCode, townName, countrySubDivision, country, addressLine
        case addressDescription = "description"
        case geoLocation = "GeoLocation"
    }
}

// MARK: - GeoLocation
struct GeoLocationBank: Codable {
    let geographicCoordinates: GeographicCoordinatesBank

    enum CodingKeys: String, CodingKey {
        case geographicCoordinates = "GeographicCoordinates"
    }
}

// MARK: - GeographicCoordinates
struct GeographicCoordinatesBank: Codable {
    let latitude, longitude: String
}

// MARK: - Information
struct Information: Codable {
    let segment: Segment
    let availability: AvailabilityBank
    let contactDetails: ContactDetailsBank
//
    enum CodingKeys: String, CodingKey {
        case segment
        case availability = "Availability"
        case contactDetails = "ContactDetails"
    }
}

// MARK: - Availability
struct AvailabilityBank: Codable {
    let access24Hours, isRestricted, sameAsOrganization: Int
    let availabilityDescription: String
    let standardAvailability: StandardAvailabilityBank
    let nonStandardAvailability: [NonStandardAvailability]

    enum CodingKeys: String, CodingKey {
        case access24Hours, isRestricted, sameAsOrganization
        case availabilityDescription = "description"
        case standardAvailability = "StandardAvailability"
        case nonStandardAvailability = "NonStandardAvailability"
    }
}

// MARK: - NonStandardAvailability
struct NonStandardAvailability: Codable {
    let name: String
    let fromDate, toDate: String
    let nonStandardAvailabilityDescription: NonStandardAvailabilityDescription?
    let day: NonStandardAvailabilityDay

    enum CodingKeys: String, CodingKey {
        case name, fromDate, toDate
        case nonStandardAvailabilityDescription = "description"
        case day = "Day"
    }
}

// MARK: - NonStandardAvailabilityDay
struct NonStandardAvailabilityDay: Codable {
    let dayCode, openingTime, closingTime: String?
    let dayBreak: BreakBank

    enum CodingKeys: String, CodingKey {
        case dayCode, openingTime, closingTime
        case dayBreak = "Break"
    }
}

// MARK: - Break
struct BreakBank: Codable {
    let breakFromTime: String
    let breakToTime: String
}

enum NonStandardAvailabilityDescription: String, Codable {
    case empty = ""
    case отделениеНеРаботает = "Отделение не работает"
}

// MARK: - StandardAvailability
struct StandardAvailabilityBank: Codable {
    let day: [DayElement]

    enum CodingKeys: String, CodingKey {
        case day = "Day"
    }
}

// MARK: - DayElement
struct DayElement: Codable {
    let dayCode: Int
    let openingTime: String
    let closingTime: String
    let dayBreak: BreakBank

    enum CodingKeys: String, CodingKey {
        case dayCode, openingTime, closingTime
        case dayBreak = "Break"
    }
}

// MARK: - ContactDetails
struct ContactDetailsBank: Codable {
    let name, phoneNumber, mobileNumber, faxNumber: String
    let emailAddress, other: String
    let socialNetworks: [SocialNetwork]

    enum CodingKeys: String, CodingKey {
        case name, phoneNumber, mobileNumber, faxNumber, emailAddress, other
        case socialNetworks = "SocialNetworks"
    }
}

// MARK: - SocialNetwork
struct SocialNetwork: Codable {
    let networkName: NetworkName
    let url: String
    let socialNetworkDescription: SocialNetworkDescription

    enum CodingKeys: String, CodingKey {
        case networkName
        case url
        case socialNetworkDescription = "description"
    }
}

enum NetworkName: String, Codable {
    case facebook = "Facebook"
    case instagram = "Instagram"
    case ok = "OK"
    case telegram = "Telegram"
    case twitter = "Twitter"
    case viber = "Viber"
    case vk = "VK"
}

enum SocialNetworkDescription: String, Codable {
    case официальнаяСтраницаБеларусбанка = "Официальная страница Беларусбанка"
}

enum Segment: String, Codable {
    case business = "Business"
    case individual = "Individual"
}

// MARK: - Services
struct ServicesBank: Codable {
    var service: ServiceBank

    enum CodingKeys: String, CodingKey {
        case service = "Service"
    }
}

// MARK: - Service
struct ServiceBank: Codable {
    let the0, the1, the2, the3: The0?
    let the4, the5, the6, the7: The0?
    let the8, the9, the10, the11: The0?
    let the12, the13, the14, the15: The0?
    let the16, the17, the18, the19: The0?
    let the20, the21, the22, the23: The0?
    let the24, the25, the26, the27: The0?
    let the28, the29, the30, the31: The0?
    let the32, the33, the34, the35: The0?
    let the36, the37, the38, the39: The0?
    let the40, the41, the42, the43: The0?
    let the44, the45, the46, the47: The0?
    let the48, the49, the50, the51: The0?
    let the52, the53, the54, the55: The0?
    let the56, the57, the58, the59: The0?
    let the60, the61, the62, the63: The0?
    let the64, the65, the66, the67: The0?
    let the68, the69, the70, the71: The0?
    let the72, the73, the74, the75: The0?
    let the76, the77, the78, the79: The0?
    let the80, the81, the82, the83: The0?
    let the84, the85, the86, the87: The0?
    let the88, the89, the90, the91: The0?
    let the92, the93, the94, the95: The0?
    let the96, the97, the98, the99: The0?
    let the100, the101, the102, the103: The0?
    var currencyExchange: [CurrencyExchange]

    enum CodingKeys: String, CodingKey {
        case the0 = "0"
        case the1 = "1"
        case the2 = "2"
        case the3 = "3"
        case the4 = "4"
        case the5 = "5"
        case the6 = "6"
        case the7 = "7"
        case the8 = "8"
        case the9 = "9"
        case the10 = "10"
        case the11 = "11"
        case the12 = "12"
        case the13 = "13"
        case the14 = "14"
        case the15 = "15"
        case the16 = "16"
        case the17 = "17"
        case the18 = "18"
        case the19 = "19"
        case the20 = "20"
        case the21 = "21"
        case the22 = "22"
        case the23 = "23"
        case the24 = "24"
        case the25 = "25"
        case the26 = "26"
        case the27 = "27"
        case the28 = "28"
        case the29 = "29"
        case the30 = "30"
        case the31 = "31"
        case the32 = "32"
        case the33 = "33"
        case the34 = "34"
        case the35 = "35"
        case the36 = "36"
        case the37 = "37"
        case the38 = "38"
        case the39 = "39"
        case the40 = "40"
        case the41 = "41"
        case the42 = "42"
        case the43 = "43"
        case the44 = "44"
        case the45 = "45"
        case the46 = "46"
        case the47 = "47"
        case the48 = "48"
        case the49 = "49"
        case the50 = "50"
        case the51 = "51"
        case the52 = "52"
        case the53 = "53"
        case the54 = "54"
        case the55 = "55"
        case the56 = "56"
        case the57 = "57"
        case the58 = "58"
        case the59 = "59"
        case the60 = "60"
        case the61 = "61"
        case the62 = "62"
        case the63 = "63"
        case the64 = "64"
        case the65 = "65"
        case the66 = "66"
        case the67 = "67"
        case the68 = "68"
        case the69 = "69"
        case the70 = "70"
        case the71 = "71"
        case the72 = "72"
        case the73 = "73"
        case the74 = "74"
        case the75 = "75"
        case the76 = "76"
        case the77 = "77"
        case the78 = "78"
        case the79 = "79"
        case the80 = "80"
        case the81 = "81"
        case the82 = "82"
        case the83 = "83"
        case the84 = "84"
        case the85 = "85"
        case the86 = "86"
        case the87 = "87"
        case the88 = "88"
        case the89 = "89"
        case the90 = "90"
        case the91 = "91"
        case the92 = "92"
        case the93 = "93"
        case the94 = "94"
        case the95 = "95"
        case the96 = "96"
        case the97 = "97"
        case the98 = "98"
        case the99 = "99"
        case the100 = "100"
        case the101 = "101"
        case the102 = "102"
        case the103 = "103"
        case currencyExchange = "CurrencyExchange"
    }
}

// MARK: - CurrencyExchange
struct CurrencyExchange: Codable {
    let exchangeTypeStaticType: ExchangeTypeStaticType
    let sourceCurrency: CurrencyBank?
    let exchangeRate: String
    let direction: Direction
    let scaleCurrency: String

    enum CodingKeys: String, CodingKey {
        case exchangeTypeStaticType = "ExchangeTypeStaticType"
        case exchangeRate, scaleCurrency, direction, sourceCurrency
    }
}

enum Direction: String, Codable {
    case buy = "buy"
    case sell = "sell"
}

enum ExchangeTypeStaticType: String, Codable {
    case cash = "Cash"
    case cashless = "Cashless"
}

enum CurrencyBank: String, Codable {
    case byn = "BYN"
    case cad = "CAD"
    case chf = "CHF"
    case cny = "CNY"
    case czk = "CZK"
    case eur = "EUR"
    case gbp = "GBP"
    case jpy = "JPY"
    case nok = "NOK"
    case pln = "PLN"
    case rub = "RUB"
    case sek = "SEK"
    case uah = "UAH"
    case usd = "USD"
}

// MARK: - The0
struct The0: Codable {
    let serviceID: ServiceID
    let name: String
    let segment: Segment
    let url: String
    let currentStatus: CurrentStatusBank
    let the0_Description: String

    enum CodingKeys: String, CodingKey {
        case serviceID = "serviceId"
        case name, url, segment, currentStatus
        case the0_Description = "description"
    }
}

enum CurrentStatusBank: String, Codable {
    case active = "Active"
    case inactive = "Inactive"
}

enum ServiceID: String, Codable {
    case strahovanieMedRashodov = "strahovanie_med_rashodov"
    case uslBroker = "usl_broker"
    case uslBuySlitki = "usl_buy_slitki"
    case uslCardInternet = "usl_card_internet"
    case uslCennieBumagi = "usl_cennie_bumagi"
    case uslCheckDoverVnebanka = "usl_check_dover_vnebanka"
    case uslChekiGilie = "usl_cheki_gilie"
    case uslChekiImuschestvo = "usl_cheki_imuschestvo"
    case uslClubBarhat = "usl_club_barhat"
    case uslClubKartblansh = "usl_club_kartblansh"
    case uslClubLedi = "usl_club_ledi"
    case uslClubNastart = "usl_club_nastart"
    case uslClubPersona = "usl_club_persona"
    case uslClubSchodry = "usl_club_schodry"
    case uslCoinsExchange = "usl_coins_exchange"
    case uslDepDoverennosti = "usl_dep_doverennosti"
    case uslDepScheta = "usl_dep_scheta"
    case uslDepViplati = "usl_dep_viplati"
    case uslDepositariy = "usl_depositariy"
    case uslDocObligacBelarusbank = "usl_docObligac_belarusbank"
    case uslDoverUpr = "usl_dover_upr"
    case uslDoverUprGos = "usl_dover_upr_gos"
    case uslDragMetal = "usl_drag_metal"
    case uslIbank = "usl_ibank"
    case uslInkassoPriem = "usl_inkasso_priem"
    case uslInkassoPriemDenegBel = "usl_inkasso_priem_deneg_bel"
    case uslInkassoVyplata = "usl_inkasso_vyplata"
    case uslIntCards = "usl_int_cards"
    case uslIzbizSchetaOperacii = "usl_izbiz_scheta_operacii"
    case uslIzbizSchetaOtkr = "usl_izbiz_scheta_otkr"
    case uslKamniBrill = "usl_kamni_brill"
    case uslKompleksnyProduct = "usl_kompleksny_product"
    case uslKonversiyaForeignVal = "usl_konversiya_foreign_val"
    case uslLoterei = "usl_loterei"
    case uslMoRb = "usl_mo_rb"
    case uslOperationsBezdokumentarObligacii = "usl_operations_bezdokumentar_obligacii"
    case uslOperationsPoSchetamBelpochta = "usl_operations_po_schetam_belpochta"
    case uslOperationsSberSertif = "usl_operations_sber_sertif"
    case uslPerechisleniePoRekvizitamKartochki = "usl_perechislenie_po_rekvizitam_kartochki"
    case uslPlategi = "usl_plategi"
    case uslPlategiAll = "usl_plategi_all"
    case uslPlategiInForeignVal = "usl_plategi_in_foreign_val"
    case uslPlategiMinusInternet = "usl_plategi_minus_internet"
    case uslPlategiMinusMobi = "usl_plategi_minus_mobi"
    case uslPlategiMinusMobiInternetFull = "usl_plategi_minus_mobi_internet_full"
    case uslPlategiNalMinusKromeKredit = "usl_plategi_nal_minus_krome_kredit"
    case uslPlategiZaProezdVPolzuBanka = "usl_plategi_za_proezd_v_polzu_banka"
    case uslPodlinnostBanknot = "usl_podlinnost_banknot"
    case uslPogashenieDocumentarObligacii = "usl_pogashenie_documentar_obligacii"
    case uslPopolnenieSchetaBezKart = "usl_popolnenieSchetaBezKart"
    case uslPopolnenieSchetaBynISPKarts = "usl_popolnenieSchetaBynIspKarts"
    case uslPopolnenieSchetaUsdISPKarts = "usl_popolnenieSchetaUsdIspKarts"
    case uslPov = "usl_pov"
    case uslPriemCennosteiNaHranenie = "usl_priem_cennostei_na_hranenie"
    case uslPriemCennostejNaHranenie = "usl_priem_cennostej_na_hranenie"
    case uslPriemDocNaKreditsMagnit = "usl_priem_doc_na_kredits_magnit"
    case uslPriemDocNaKreditsOverdrafts = "usl_priem_doc_na_kredits_overdrafts"
    case uslPriemDocNaLizing = "usl_priem_doc_na_lizing"
    case uslPriemDocPokupkaObl = "usl_priemDocPokupkaObl"
    case uslPriemDocsFLDepozitOperations = "usl_priem_docs_fl_depozit_operations"
    case uslPriemDocsVidachaSoprLgotIpotech = "usl_priem_docs_vidacha_sopr_lgot_ipotech"
    case uslPriemInkasso = "usl_priem_inkasso"
    case uslPriemOblMF = "usl_priem_obl_mf"
    case uslPriemPlatejeiBynIP = "usl_priem_platejei_byn_ip"
    case uslPriemPlatejeiEurIP = "usl_priem_platejei_eur_ip"
    case uslPriemVznosovInostrValOtStraxAgentov = "usl_priem_vznosov_inostr_val_ot_strax_agentov"
    case uslPriemZayvleniyObsluzhivanieDerzhatelej = "usl_priem_zayvleniy_obsluzhivanie_derzhatelej"
    case uslProdagaMonet = "usl_prodaga_monet"
    case uslPupilCard = "usl_pupil_card"
    case uslRazmProdazhaDocumentarObligacii = "usl_razm_prodazha_documentar_obligacii"
    case uslRazmenForeignVal = "usl_razmen_foreign_val"
    case uslRbCard = "usl_rb_card"
    case uslRegistrationValDogovor = "usl_registration_val_dogovor"
    case uslReturnBynISPKarts = "usl_return_BynIspKarts"
    case uslReturnUsdISPKarts = "usl_return_UsdIspKarts"
    case uslRko = "usl_rko"
    case uslSeif = "usl_seif"
    case uslStrahovanieAvto = "usl_strahovanie_avto"
    case uslStrahovanieAvtoPogran = "usl_strahovanie_avto_pogran"
    case uslStrahovanieDetei = "usl_strahovanie_detei"
    case uslStrahovanieDohodPodZaschitoy = "usl_strahovanie_dohod_pod_zaschitoy"
    case uslStrahovanieExpress = "usl_strahovanie_express"
    case uslStrahovanieGreenKarta = "usl_strahovanie_green_karta"
    case uslStrahovanieHome = "usl_strahovanie_home"
    case uslStrahovanieKartochki = "usl_strahovanie_kartochki"
    case uslStrahovanieKasko = "usl_strahovanie_kasko"
    case uslStrahovanieKomplex = "usl_strahovanie_komplex"
    case uslStrahovanieMedicineNerezident = "usl_strahovanie_medicine_nerezident"
    case uslStrahovaniePerevozki = "usl_strahovanie_perevozki"
    case uslStrahovanieSZabotoiOBlizkih = "usl_strahovanie_s_zabotoi_o_blizkih"
    case uslStrahovanieTimeAbroad = "usl_strahovanie_timeAbroad"
    case uslStrahovanieZashhitaOtKleshha = "usl_strahovanie_zashhita_ot_kleshha"
    case uslStrahovkaSite = "usl_strahovka_site"
    case uslStroysber = "usl_stroysber"
    case uslStroysberNew = "usl_stroysber_new"
    case uslSubsidiyaScheta = "usl_subsidiya_scheta"
    case uslSwift = "usl_swift"
    case uslVklad = "usl_vklad"
    case uslVozvratNDS = "usl_vozvrat_nds"
    case uslVydachaNalVBanke = "usl_vydacha_nal_v_banke"
    case uslVydachaVypiski = "usl_vydacha_vypiski"
    case uslVypllataBelRub = "usl_vypllata_bel_rub"
    case uslVzk = "usl_vzk"
    case uslWu = "usl_wu"
}

