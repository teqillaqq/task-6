//
//  GruppedByCytyObject.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 28.02.22.
//

import Foundation

struct GruppedByCytyObject {
    var sectionName: String
    var sectionObjects: [AnnotationsArray]
}
