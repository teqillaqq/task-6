//
//  BelarusbankDataATM+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 5.03.22.
//

import Foundation

extension ATM {

    static func getAtmAddress(atm: ATM) -> String {
        return atm.address.townName.isEmpty ?
        ("Адрес: \n" + atm.address.countrySubDivision.rawValue +
         " область, улица " + atm.address.streetName +
         ", дом " + atm.address.buildingNumber) :
        ("Адрес: " + atm.address.countrySubDivision.rawValue +
         " область, " + atm.address.townName +
         ", улица " + atm.address.streetName + ", дом " +
         atm.address.buildingNumber)
    }

    static func getAtmworkingHours(atm: ATM) -> String {
        if atm.availability.standardAvailability.day.isEmpty {
            return "На данный момент банкомат не работает"
        } else {
            var listOfDays = "Время работы:"
            var index = 0
            for day in atm.availability.standardAvailability.day {
                listOfDays.append(" \n\(day.dayCode.convertDayCodeToDay(dayCode: day.dayCode)) - с: " +
                                  atm.availability.standardAvailability.day[index].openingTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].closingTime.rawValue +
                                  ", обед с: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakFromTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakToTime.rawValue + ";")
                while index < atm.availability.standardAvailability.day.count - 1 {
                    index += 1
                }
            }
            return listOfDays
        }
    }

    static func getIssuedCurrency(atm: ATM) -> String {
        return "Выдаваемая валюта: " + atm.currency.rawValue
    }

    static func getCashIn(atm: ATM) -> String {
        var cashIn = ""
        for service in atm.services {
            cashIn.append(" " + service.serviceType.rawValue + ",")
        }
        if cashIn.contains("CashIn") {
            cashIn = "CashIn поддерживает"
        } else {
            cashIn = "CashIn не поддерживает"
        }
        return cashIn
    }

    static func getCity(atm: ATM) -> String {
        if atm.address.townName.isEmpty {
            return ""
        } else {
            return atm.address.townName
        }
    }
}
