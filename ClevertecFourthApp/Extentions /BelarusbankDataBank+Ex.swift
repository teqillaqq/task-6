//
//  BelarusbankDataBank+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 6.03.22.
//

import Foundation

extension Branch {

    static func getCity(bankData: Branch) -> String {
        return bankData.address.townName
    }

    static func getAddress(bankData: Branch) -> String {
        return "Адрес: \n" + "город " + bankData.address.townName +
         ", улица " + bankData.address.streetName +
        ", дом " + bankData.address.buildingNumber
    }

    static func getworkingHoursBank(bankData: Branch) -> String {
        if bankData.information.availability.standardAvailability.day.isEmpty {
            return "На данный момент отделение банка не работает"
        } else {
            var listOfDays = "Время работы:"
            var index = 0
            for day in bankData.information.availability.standardAvailability.day {
                listOfDays.append(" \n\(String(describing: day.dayCode.convertDayCodeToDay(dayCode: (day.dayCode))))" +
                                  " с: " + day.openingTime +
                                  "GMT до: " +
                                  day.closingTime +
                                  "GMT, обед с: " +
                                  day.dayBreak.breakFromTime  +
                                  "GMT до: " +
                                  day.dayBreak.breakToTime + "GMT;")
                while index < bankData.information.availability.standardAvailability.day.count - 1 {
                    index += 1
                }
            }
            return listOfDays
        }
    }

    static func getIssuedCurrency(bankData: Branch) -> String {
        if bankData.services.service.currencyExchange.isEmpty {
            return "Нет данных о валюте"
        } else {
            var listOfCurrency = "Доступные валюты:"
            for currency in bankData.services.service.currencyExchange {
                listOfCurrency.append(currency.sourceCurrency?.rawValue ?? "")
                listOfCurrency.append(", ")
            }
            return listOfCurrency
        }
    }
    
    static func getCashIn(bankData: Branch) -> String {
        return "Принимает наличные"
    }

    static func getaAccountBumber(bankData: Branch) -> String {
        guard let accountNumber = bankData.accountNumber?.rawValue else
        { return "Нет номера счета отделения банка"}
        return "Номер счета отделения банка: " + accountNumber
    }

    static func getaEqueueAndWifiLabel(bankData: Branch) -> String {
        var equeue = ""
        var wifi = ""
        if bankData.wifi == 1 {
            wifi.append("В отделении банка есть wify\n")
        } else {
            wifi.append("В отделении банка нет wify\n")
        }
        if bankData.equeue == 1 {
            equeue.append("В отделении банка есть электронная очередь")
        } else {
            equeue.append("В отделении банка нет электронной очереди")
        }
        return wifi + equeue
    }

    static func getCoordinate(bankData: Branch) -> String {
        return "Координаты отделения банка:\n" +
        "широта - " + bankData.address.geoLocation.geographicCoordinates.latitude +
        ",\nдолгота - " + bankData.address.geoLocation.geographicCoordinates.longitude
    }

    static func getinformation(bankData: Branch) -> String {
        return  "\nтелефоны: " + bankData.information.contactDetails.phoneNumber +
        " " + bankData.information.contactDetails.faxNumber +
        " " + bankData.information.contactDetails.mobileNumber
    }

    static func getSegment(bankData: Branch) -> String {
        switch bankData.information.segment.rawValue {
        case "Business":
            return "Отделение работает с юридическими лицами"
        case "Individual":
            return "Отделение работает с физическими лицами"
        default:
            return "Отделение работает с физическими и юридическими лицами"
        }
    }

    static func getadditionalInformation(bankData: Branch) -> String {
        var access24Hours = ""
        var sameAsOrganization = ""
        var isRestricted = ""
        if bankData.information.availability.access24Hours == 1 {
            access24Hours = "Отделение доступно круглосуточно\n"
        } else {
            access24Hours = "Отделение не доступно круглосуточно\n"
        }
        if bankData.information.availability.sameAsOrganization == 1 {
            sameAsOrganization = "Доступ к отделению ограничен графиком работы организации\n"
        } else {
            sameAsOrganization = "Доступ к отделению не ограничен графиком работы организации\n"
        }
        if bankData.information.availability.isRestricted == 1 {
            isRestricted = "Доступ к отделению ограничен"
        } else {
            isRestricted = "Доступ к отделению не ограничен"
        }
        return access24Hours + sameAsOrganization + isRestricted
    }

    static func getServices(bankData: Branch) -> String {
        var counter = 1
        var currencyExchange = "Курсы валют:\n"

        for currency in bankData.services.service.currencyExchange {
            if counter < 7 {
                switch counter {
                case 1:
                    currencyExchange.append("\nДоллар США покупка")
                case 2:
                    currencyExchange.append("\nДоллар США продажа")
                case 3:
                    currencyExchange.append("\nЕвро покупка")
                case 4:
                    currencyExchange.append("\nЕвро продажа")
                case 5:
                    currencyExchange.append("\nРоссийский рубль100 покупка")
                case 6:
                    currencyExchange.append("\nРоссийский рубль100 продажа")
                default:
                    currencyExchange.append("\nНеизвестная валюта")
                }
                counter += 1
                currencyExchange.append("\n" + currency.exchangeRate)
            }
        }
        return currencyExchange
    }
}
