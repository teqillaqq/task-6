//
//  BelarusbankDataInfobox+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 5.03.22.
//

import Foundation

extension BelarusbankDataInfobox {

    static func getCity(infobox: BelarusbankDataInfobox) -> String {
        return infobox.city
    }

    static func getAddressgetCity(infobox: BelarusbankDataInfobox) -> String {
        return "Адрес: \n" + infobox.area.rawValue + " область, " + infobox.cityType.rawValue + " " +
        infobox.city + ", " + infobox.addressType.rawValue + infobox.address +  ", дом " +
        infobox.house
    }

    static func getCashIn(infobox: BelarusbankDataInfobox) -> String {
        return "Наличие купюроприемника: " + infobox.cashInExist.rawValue
    }
    
    static func getcashInInfo(infobox: BelarusbankDataInfobox) -> String {
        return "Исправность купюроприемника: " + infobox.cashIn.rawValue + "\n\n" +
        "Приёмник пачек банкнот: " + infobox.typeCashIn.rawValue
    }

    static func getworkingHours(infobox: BelarusbankDataInfobox) -> String {
        return "Время работы:\n" + infobox.workTime
    }

    static func getworkingHoursFull(infobox: BelarusbankDataInfobox) -> String {
        return "Время работы:\n" + infobox.timeLong
    }

    static func getCurrencyl(infobox: BelarusbankDataInfobox) -> String {
        return "Выдаваемая валюта: " + infobox.currency.rawValue
    }

    static func getinfType(infobox: BelarusbankDataInfobox) -> String {
        return "Тип инфокиоска по месту установки:\n" + infobox.infType.rawValue
    }

    static func getinfPrinter(infobox: BelarusbankDataInfobox) -> String {
        return "Возможность печати чека (исправность чекового принтера):\n" +
        infobox.infPrinter.rawValue
    }

    static func getregionPlatej(infobox: BelarusbankDataInfobox) -> String {
        return "Региональные платежи: " + infobox.regionPlatej.rawValue
    }

    static func getpopolneniePlatej(infobox: BelarusbankDataInfobox) -> String {
        return "Пополнение картсчета наличными: " + infobox.popolneniePlatej.rawValue
    }

    static func getInfStatus(infobox: BelarusbankDataInfobox) -> String {
        return "Исправность инфокиоска: " + infobox.infStatus.rawValue
    }

    static func getCoordinate(infobox: BelarusbankDataInfobox) -> String {
        return "Координаты банкомата:\n" +
        "широта - " + infobox.gpsX +
        ",\nдолгота - " + infobox.gpsY
    }
}
