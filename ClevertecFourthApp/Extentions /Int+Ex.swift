//
//  Int+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 7.03.22.
//

import Foundation

extension Int {

    func convertDayCodeToDay(dayCode: Int) -> String {
        switch dayCode {
        case 1:
            return "понедельник"
        case 2:
            return "вторник"
        case 3:
            return "среда"
        case 4:
            return "четверг"
        case 5:
            return "пятница"
        case 6:
            return "суббота"
        case 7:
            return "воскресенье"
        default:
            return "неизвестный "
        }
    }
}
