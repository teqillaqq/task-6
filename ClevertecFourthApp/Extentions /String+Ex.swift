//
//  String+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 28.02.22.
//

import Foundation

extension String {

    func convertDayCodeToDay(dayCode: String) -> String {
        switch dayCode {
        case "01":
            return "понедельник"
        case "02":
            return "вторник"
        case "03":
            return "среда"
        case "04":
            return "четверг"
        case "05":
            return "пятница"
        case "06":
            return "суббота"
        case "07":
            return "воскресенье"
        default:
            return "неизвестный "
        }
    }
}
