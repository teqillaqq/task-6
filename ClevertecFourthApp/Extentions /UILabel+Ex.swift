//
//  UILabel+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 2.03.22.
//

import UIKit

extension UILabel {
    static func makeLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        return label
    }
}
