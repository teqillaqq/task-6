//
//  UIViewController+Ex.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 27.02.22.
//

import Foundation
import UIKit

extension UIViewController {

    func alertError(with error: Error, titleOfData: String) {
        let alert = UIAlertController(title: ContentConstants.error,
                                      message: ContentConstants.failedToLoad +
                                      titleOfData,
                                      preferredStyle: .alert)
        let reloadNetwork = UIAlertAction(title: ContentConstants.pressTheTefreshButton,
                                          style: .default) { _ in
        }
        let okAction = UIAlertAction(title: ContentConstants.close, style: .default)
        alert.addAction(reloadNetwork)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }

    func showSettingsAlert() {
        let alert = UIAlertController(title: nil, message: ContentConstants.openPhoneSettings,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: ContentConstants.openSettings,
                                      style: .default) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        alert.addAction(UIAlertAction(title: ContentConstants.change,
                                      style: .cancel) { _ in
        })
        present(alert, animated: true)
    }
}
