//
//  SceneDelegate.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        let startVC = StartViewController()
        let startNC = UINavigationController(rootViewController: startVC)
        window?.rootViewController = startNC
        window?.makeKeyAndVisible()
    }
}

