//
//  AtmPopUpView.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 24.02.22.
//


import Foundation
import UIKit
import MapKit

class PopUpWindow: UIViewController {

    private let popUpWindowView = PopUpWindowView()
    var atmData: ATM?
    var infoboxData: BelarusbankDataInfobox?
    var bankData: Branch?
    var selectedAnatation: AnnotationsArray?

    init(title: String, atmAnnotations: AnnotationsArray) {
        super.init(nibName: nil, bundle: nil)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        popUpWindowView.popupTitle.text = title
        popUpWindowView.configure(atmAnnotations: atmAnnotations)
        popUpWindowView.showInfoButton.addTarget(self, action: #selector(showInfoViewController),
                                                 for: .touchUpInside)
        popUpWindowView.closeButton.addTarget(self, action: #selector(dismissPopView),
                                              for: .touchUpInside)
        view = popUpWindowView
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.popUpWindowView.popupView {
            self.dismiss(animated: true, completion: nil)
        }
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func showInfoViewController(){
        switch selectedAnatation?.type {
        case "ATM":
            let fullInfoViewController = FullAtmInfoViewController()
            fullInfoViewController.atmData = atmData
            self.present(fullInfoViewController, animated: true, completion: nil)
        case "Infobox":
            let fullInfoViewController = FullInfoboxInfoViewController()
            fullInfoViewController.infoboxData = infoboxData
            self.present(fullInfoViewController, animated: true, completion: nil)
        case "Bank":
            let fullInfoViewController = FullBankInfoViewController()
            fullInfoViewController.bankData = bankData
            self.present(fullInfoViewController, animated: true, completion: nil)
        default:
            print("Неизвестный тип")
        }
    }

    @objc private func dismissPopView(){
        self.dismiss(animated: true, completion: nil)
    }
}

private class PopUpWindowView: UIView {

    private let heightConstraint: CGFloat = 67
    private let borderWidth: CGFloat = 2.0
    private let sideSpaceConstraint: CGFloat = 15
    private let topSpaceConstraint: CGFloat = 8

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    lazy var popupView: UIView = {
        let popupView = UIView(frame: CGRect.zero)
        popupView.backgroundColor = .white
        popupView.layer.borderWidth = 2
        popupView.layer.masksToBounds = true
        popupView.layer.borderColor = UIColor.white.cgColor
        return popupView
    }()

    lazy var popupTitle: UILabel = {
        let popupTitle = UILabel(frame: CGRect.zero)
        popupTitle.textColor = UIColor.white
        popupTitle.backgroundColor = ColorConstants.background
        popupTitle.layer.masksToBounds = true
        popupTitle.adjustsFontSizeToFitWidth = true
        popupTitle.clipsToBounds = true
        popupTitle.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        popupTitle.numberOfLines = 0
        popupTitle.textAlignment = .center
        return popupTitle
    }()

    private let commonInformationLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        return label
    }()

    private let workingHoursLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizeOfCollectionView)
        return label
    }()

    lazy var closeButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.tintColor = ColorConstants.background
        return button
    }()

    lazy var showInfoButton: UIButton = {
        let popupButton = UIButton(frame: CGRect.zero)
        popupButton.setTitleColor(UIColor.white, for: .normal)
        popupButton.setTitle(ContentConstants.description, for: .normal)
        popupButton.titleLabel?.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        popupButton.backgroundColor = ColorConstants.background
        return popupButton
    }()

    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = UIColor.black.withAlphaComponent(0.3)

        addSubview(popupView)
        popupView.addSubview(showInfoButton)
        popupView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(50)
            make.trailing.equalToSuperview().offset(-50)
            make.centerX.centerY.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            make.bottom.equalToSuperview().offset(-100)
        }

        popupView.addSubview(popupTitle)
        popupTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(borderWidth)
            make.trailing.equalToSuperview().offset(-52)
            make.top.equalToSuperview().offset(borderWidth)
            make.height.equalTo(55)
        }

        popupView.addSubview(closeButton)
        closeButton.snp.makeConstraints { make in
            make.leading.equalTo(popupTitle.snp.trailing)
            make.top.equalToSuperview().offset(borderWidth)
            make.trailing.equalToSuperview().offset(-borderWidth)
            make.bottom.equalTo(popupTitle.snp.bottom)
        }

        popupView.addSubview(commonInformationLabel)
        commonInformationLabel.snp.makeConstraints { make in
            make.height.greaterThanOrEqualTo(heightConstraint)
            make.top.equalTo(popupTitle.snp.bottom).offset(topSpaceConstraint)
            make.leading.equalToSuperview().offset(sideSpaceConstraint)
            make.trailing.equalToSuperview().offset(-sideSpaceConstraint)
        }

        popupView.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.height.greaterThanOrEqualTo(67)
            make.top.equalTo(commonInformationLabel.snp.bottom).offset(topSpaceConstraint)
            make.leading.equalToSuperview().offset(sideSpaceConstraint)
            make.trailing.equalToSuperview().offset(-sideSpaceConstraint)
            make.bottom.equalTo(showInfoButton.snp.top).offset(-8)
        }

        scrollView.addSubview(workingHoursLabel)
        workingHoursLabel.snp.makeConstraints { make in
            make.leading.top.trailing.bottom.equalToSuperview()
            make.centerX.equalTo(scrollView.snp.centerX)
            make.width.equalTo(scrollView.snp.width)
            make.top.equalTo(scrollView.snp.top)
            make.bottom.equalTo(scrollView.snp.bottom)
        }



        showInfoButton.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.equalToSuperview().offset(borderWidth)
            make.trailing.equalToSuperview().offset(-borderWidth)
            make.bottom.equalToSuperview().offset(-borderWidth)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(atmAnnotations: AnnotationsArray) {
        commonInformationLabel.text = atmAnnotations.address + "\n" +
        atmAnnotations.cashIn + "\n" +
        atmAnnotations.issuedCurrency
        workingHoursLabel.text = atmAnnotations.workingHours
    }
}

