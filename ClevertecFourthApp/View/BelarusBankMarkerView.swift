//
//  BelarusBankMarkerView.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 3.03.22.
//
import Foundation
import MapKit

class BelarusBankMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let belarusBankannotation = newValue as? AnnotationsArray else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            markerTintColor = belarusBankannotation.markerTintColor
            glyphImage = belarusBankannotation.image
        }
    }
}
