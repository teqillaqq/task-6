//
//  FilterPopUpView.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 7.03.22.
//

import Foundation
import UIKit

class FilterPopUpView: UIViewController {

    private let popUpWindowView = PopUpWindowView()

    init(title: String) {
        super.init(nibName: nil, bundle: nil)
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        popUpWindowView.popupTitle.text = title
        popUpWindowView.closeButton.addTarget(self, action: #selector(dismissPopView),
                                              for: .touchUpInside)
        view = popUpWindowView
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.popUpWindowView.popupView {
            self.dismiss(animated: true, completion: nil)
        }
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func dismissPopView() {
        self.dismiss(animated: true, completion: nil)
    }
}

private class PopUpWindowView: UIView {
    private let heightConstraint: CGFloat = 67
    private let borderWidth: CGFloat = 2.0
    private let sideSpaceConstraint: CGFloat = 15
    private let topSpaceConstraint: CGFloat = 8
    private var counter = 0 {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "int"),
                                            object: nil, userInfo: ["int": counter])
        }
    }

    lazy var popupView: UIView = {
        let popupView = UIView(frame: CGRect.zero)
        popupView.backgroundColor = .white
        popupView.layer.borderWidth = 2
        popupView.layer.masksToBounds = true
        popupView.layer.borderColor = UIColor.white.cgColor
        return popupView
    }()

    lazy var popupTitle: UILabel = {
        let popupTitle = UILabel(frame: CGRect.zero)
        popupTitle.textColor = UIColor.white
        popupTitle.backgroundColor = ColorConstants.background
        popupTitle.layer.masksToBounds = true
        popupTitle.adjustsFontSizeToFitWidth = true
        popupTitle.clipsToBounds = true
        popupTitle.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        popupTitle.numberOfLines = 0
        popupTitle.textAlignment = .center
        return popupTitle
    }()

    private let commonInformationLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "Банкоматы\nИнфокиоски\nОтделения банка"
        label.font = .boldSystemFont(ofSize: TextConstants.fontSizePopup)
        return label
    }()

    lazy var closeButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.tintColor = ColorConstants.background
        return button
    }()

    lazy var tickBoxAtm: Checkbox = {
        tickBoxAtm = Checkbox()
        tickBoxAtm.borderStyle = .square
        tickBoxAtm.checkmarkStyle = .tick
        tickBoxAtm.checkmarkSize = 0.7
        tickBoxAtm.valueChanged = { (value) in
            print("tickBoxAtm value change: \(value)")
            if value {
                self.counter += 1
            } else {
                self.counter -= 1
            }
        }
        return tickBoxAtm
    }()

    lazy var tickBoxInfobox: Checkbox = {
        tickBoxInfobox = Checkbox()
        tickBoxInfobox.borderStyle = .square
        tickBoxInfobox.checkmarkStyle = .tick
        tickBoxInfobox.checkmarkSize = 0.7
        tickBoxInfobox.valueChanged = { (value) in
            print("tickBoxInfobox value change: \(value)")
            if value {
                self.counter += 10
            } else {
                self.counter -= 10
            }
        }
        return tickBoxInfobox
    }()

    lazy var tickBoxBank: Checkbox = {
        tickBoxBank = Checkbox()
        tickBoxBank.borderStyle = .square
        tickBoxBank.checkmarkStyle = .tick
        tickBoxBank.checkmarkSize = 0.7
        tickBoxBank.valueChanged = { (value) in
            print("tickBoxBank value change: \(value)")
            if value {
                self.counter += 100
            } else {
                self.counter -= 100
            }
        }
        return tickBoxBank
    }()

    init() {
        super.init(frame: CGRect.zero)
        backgroundColor = UIColor.black.withAlphaComponent(0.3)

        addSubview(popupView)
        popupView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(50)
            make.trailing.equalToSuperview().offset(-50)
            make.centerX.centerY.equalToSuperview()
            make.top.equalToSuperview().offset(150)
            make.bottom.equalToSuperview().offset(-150)
        }

        popupView.addSubview(popupTitle)
        popupTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(borderWidth)
            make.trailing.equalToSuperview().offset(-52)
            make.top.equalToSuperview().offset(borderWidth)
            make.height.equalTo(55)
        }

        popupView.addSubview(closeButton)
        closeButton.snp.makeConstraints { make in
            make.leading.equalTo(popupTitle.snp.trailing)
            make.top.equalToSuperview().offset(borderWidth)
            make.trailing.equalToSuperview().offset(-borderWidth)
            make.bottom.equalTo(popupTitle.snp.bottom)
        }

        popupView.addSubview(commonInformationLabel)
        commonInformationLabel.snp.makeConstraints { make in
            make.height.greaterThanOrEqualTo(heightConstraint)
            make.centerX.equalToSuperview()
            make.top.equalTo(popupTitle.snp.bottom).offset(topSpaceConstraint)
            make.leading.equalToSuperview().offset(sideSpaceConstraint)
            make.trailing.equalToSuperview().offset(-sideSpaceConstraint)
        }

        popupView.addSubview(tickBoxAtm)
        tickBoxAtm.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(35)
            make.width.equalTo(35)
            make.top.equalTo(commonInformationLabel.snp.bottom).offset(topSpaceConstraint)
        }

        popupView.addSubview(tickBoxInfobox)
        tickBoxInfobox.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(35)
            make.width.equalTo(35)
            make.top.equalTo(tickBoxAtm.snp.bottom).offset(topSpaceConstraint)
        }

        popupView.addSubview(tickBoxBank)
        tickBoxBank.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(35)
            make.width.equalTo(35)
            make.top.equalTo(tickBoxInfobox.snp.bottom).offset(topSpaceConstraint)
            make.bottom.equalToSuperview().offset(-sideSpaceConstraint * 3)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

