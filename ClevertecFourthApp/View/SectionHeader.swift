//
//  SectionHeader.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 28.02.22.
//

import UIKit
import SnapKit

class SectionHeader: UICollectionReusableView {

    static let reuserID = "SectionHeader"

    lazy var titleForHeader: UILabel = {
        let title = UILabel()
        title.textAlignment = .center
        title.font = UIFont.boldSystemFont(ofSize: 18)
        return title
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SectionHeader {

    private func initialize() {
        addSubview(titleForHeader)
        titleForHeader.snp.makeConstraints { make in
            make.leading.bottom.trailing.top.equalToSuperview()
        }
    }
}
