//
//  FullAtmInfoViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 27.02.22.
//

import UIKit
import SnapKit
import MapKit

class FullAtmInfoViewController: UIViewController {

    var location: AnnotationsArray?
    var atmData: ATM?
    private let multiplier: Double = (4/5)

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    private let contentView: UIView = {
        let contentView = UIView()
        return contentView
    }()

    private let showRouteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .darkGray
        button.setTitle(ContentConstants.showRouteTitle,
                        for: .normal)
        button.layer.cornerRadius = 18
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.white.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showRoute),
                         for: .touchUpInside)
        return button
    }()

    private let locationOfTheATMabel = UILabel.makeLabel()
    private let addressLineLabel = UILabel.makeLabel()
    private let atmIdLabel = UILabel.makeLabel()
    private let availabilityLabel = UILabel.makeLabel()
    private let workingHoursLabel = UILabel.makeLabel()
    private let issuedCurrencyLabel = UILabel.makeLabel()
    private let cardsLabel = UILabel.makeLabel()
    private let currentStatusLabel = UILabel.makeLabel()
    private let servicesLabel = UILabel.makeLabel()
    private let descriptionLabel = UILabel.makeLabel()
    private let coordinatesLabel = UILabel.makeLabel()
    private let contactDetailsLabel = UILabel.makeLabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubView()
        setupScrollView()
        setupConstraints()
        setupButton()
        guard let atmData = atmData else {
            return
        }
        configure(atm: atmData)
    }

    @objc private func showRoute(location: AnnotationsArray) {
        guard let atmData = atmData else {
            return
        }
        let type = atmData.type
        let title = atmData.address.addressLine
        let locationName = atmData.atmID
        let idNumber = atmData.atmID
        let city = atmData.address.townName
        let address = ""
        let workingHours = ""
        let issuedCurrency = ""
        let cashIn = ""
        let lat = Double(atmData.address.geolocation.geographicCoordinates.latitude)
        ?? 53.5439
        let long = Double(atmData.address.geolocation.geographicCoordinates.longitude)
        ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat,
                                                longitude: long)
        let location = AnnotationsArray(type: type.rawValue, title: title, locationName: locationName, coordinate: coordinate, idNumber: idNumber, address: address, workingHours: workingHours, issuedCurrency: issuedCurrency, cashIn: cashIn, city: city)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }

    func configure(atm: ATM) {
        locationOfTheATMabel.text = atm.address.townName.isEmpty ?
        ("Адрес: \n" + atm.address.countrySubDivision.rawValue +
         " область, улица " + atm.address.streetName +
         ", дом " + atm.address.buildingNumber) :
        ("Адрес: " + atm.address.countrySubDivision.rawValue +
         " область, " + atm.address.townName +
         ", улица " + atm.address.streetName + ", дом " +
         atm.address.buildingNumber)

        addressLineLabel.text = "Место установки банкомата: \n" +
        atm.address.addressLine

        atmIdLabel.text = "ID номер банкомата: " +
        atm.atmID

        availabilityLabel.text = atm.availability.sameAsOrganization ?
        (atm.availability.access24Hours ?
         ("Доступ к банкомату круглосуточный и совпадает с режимом работы организации") :
            ("Доступ к банкомату не круглосуточный и совпадает с режимом работы организации")) :
        (atm.availability.access24Hours ?
         ("Доступ к банкомату круглосуточный, с режимом работы организации не совпадает") :
            ("Доступ к банкомату не круглосуточный, с режимом работы организации не совпадает"))

        if atm.availability.standardAvailability.day.isEmpty {
            workingHoursLabel.text = "На данный момент банкомат не работает"
        } else {
            var listOfDays = "Время работы:"
            var index = 0
            for day in atm.availability.standardAvailability.day {
                listOfDays.append(" \n\(day.dayCode.convertDayCodeToDay(dayCode: day.dayCode)) - с: " +
                                  atm.availability.standardAvailability.day[index].openingTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].closingTime.rawValue +
                                  ", обед с: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakFromTime.rawValue +
                                  " до: " +
                                  atm.availability.standardAvailability.day[index].dayBreak.breakToTime.rawValue + ";")
                if index >= atm.availability.standardAvailability.day.count { return }
                else {
                    index += 1
                }
            }
            workingHoursLabel.text = listOfDays
        }

        issuedCurrencyLabel.text = "Выдаваемая валюта: " + atm.currency.rawValue

        if atm.cards.isEmpty {
            cardsLabel.text = "На данный момент банкомат не принимает платежные карты"
        } else {
            var listOfCards = "Банкомат принимает следующие платежные карты:\n"
            for card in atm.cards {
                listOfCards.append(card.rawValue + "\n")
            }
            listOfCards.removeLast()
            cardsLabel.text = listOfCards
        }

        switch atm.currentStatus.rawValue {
        case "On":
            currentStatusLabel.text = "В данный момент банкомат работает"
        case "Off":
            currentStatusLabel.text = "В данный момент банкомат не работает"
        default:
            currentStatusLabel.text = "В данный момент нет данных, работает ли банкомат"
        }

        if atm.services.isEmpty {
            servicesLabel.text = "На данный момент нет информации об доступных услугах"
        } else {
            var listOfServices = "Банкомат предоставляет следующие услуги:\n"
            for service in atm.services {
                listOfServices.append(service.serviceType.rawValue + "\n")
            }
            servicesLabel.text = listOfServices
        }

        if atm.services.isEmpty {
            descriptionLabel.text = "На данный момент нет дополнительной информации по услугам"
        } else {
            var listOfServices = "Дополнительная информация по услугам:\n"
            for service in atm.services {
                if service.serviceDescription.rawValue == "" {
                    print("Void serviceDescription rawValue")
                } else {
                    listOfServices.append(service.serviceDescription.rawValue + "\n")
                }
            }
            listOfServices.removeLast()
            descriptionLabel.text = listOfServices
        }

        coordinatesLabel.text = "Координаты банкомата:\n" +
        "широта - " + atm.address.geolocation.geographicCoordinates.latitude +
        ",\nдолгота - " + atm.address.geolocation.geographicCoordinates.longitude

        if atm.contactDetails.phoneNumber == "" {
            contactDetailsLabel.text = "Контактный телефон не указан"
        } else {
            contactDetailsLabel.text = "Контактный телефон: " +
            atm.contactDetails.phoneNumber
        }
    }
}

extension FullAtmInfoViewController {

    private func setupScrollView() {

        scrollView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(showRouteButton.snp.top).offset(-20)
        }

        contentView.snp.makeConstraints { make in
            make.centerX.equalTo(scrollView.snp.centerX)
            make.width.equalTo(scrollView.snp.width)
            make.top.equalTo(scrollView.snp.top)
            make.bottom.equalTo(scrollView.snp.bottom)
        }
    }

    private func setupButton() {
        showRouteButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(35)
            make.trailing.bottom.equalToSuperview().offset(-35)
            make.height.equalTo(35)
        }
    }

    private func addSubView() {
        view.addSubview(scrollView)
        view.addSubview(showRouteButton)
        scrollView.addSubview(contentView)
        contentView.addSubview(locationOfTheATMabel)
        contentView.addSubview(addressLineLabel)
        contentView.addSubview(atmIdLabel)
        contentView.addSubview(availabilityLabel)
        contentView.addSubview(workingHoursLabel)
        contentView.addSubview(issuedCurrencyLabel)
        contentView.addSubview(cardsLabel)
        contentView.addSubview(currentStatusLabel)
        contentView.addSubview(servicesLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(coordinatesLabel)
        contentView.addSubview(contactDetailsLabel)
    }

    private func setupConstraints() {
        locationOfTheATMabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(contentView.snp.top).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        addressLineLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(locationOfTheATMabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        atmIdLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(addressLineLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        availabilityLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(atmIdLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        workingHoursLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(availabilityLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        issuedCurrencyLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(workingHoursLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        cardsLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(issuedCurrencyLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        currentStatusLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(cardsLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        servicesLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(currentStatusLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        descriptionLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(servicesLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        coordinatesLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(descriptionLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        contactDetailsLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(coordinatesLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
