//
//  FullBankInfoViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 6.03.22.
//

import UIKit
import SnapKit
import MapKit

class FullBankInfoViewController: UIViewController {

    var location: AnnotationsArray?
    var bankData: Branch?
    private let multiplier: Double = (4/5)

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    private let contentView: UIView = {
        let contentView = UIView()
        return contentView
    }()

    private let showRouteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .darkGray
        button.setTitle(ContentConstants.showRouteTitle,
                        for: .normal)
        button.layer.cornerRadius = 18
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.white.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showRoute),
                         for: .touchUpInside)
        return button
    }()

    private let locationOfTheATMabel = UILabel.makeLabel()
    private let addressLineLabel = UILabel.makeLabel()
    private let infoboxIdLabel = UILabel.makeLabel()
    private let workingHoursLabel = UILabel.makeLabel()
    private let issuedCurrencyLabel = UILabel.makeLabel()
    private let cbuLabel = UILabel.makeLabel()
    private let accountNumberLabel = UILabel.makeLabel()
    private let equeueAndWifiLabel = UILabel.makeLabel()
    private let         segmentLabel = UILabel.makeLabel()
    private let         additionalInformationLabel = UILabel.makeLabel()
    private let         servicesLabel = UILabel.makeLabel()
    private let informationLabel = UILabel.makeLabel()
    private let contactDetailsLabel = UILabel.makeLabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubView()
        setupScrollView()
        setupConstraints()
        setupButton()
        guard let bankData = bankData else {
            return
        }
        configure(bankData: bankData)
    }

    @objc private func showRoute(location: AnnotationsArray) {
        guard let bankData = bankData else {
            return
        }
        let type = bankData.type
        let title = bankData.name
        let locationName = bankData.name
        let idNumber = bankData.branchID
        let city = Branch.getCity(bankData: bankData)
        let address = ""
        let workingHours = ""
        let issuedCurrency = ""
        let cashIn = ""
        let lat = Double(bankData.address.geoLocation.geographicCoordinates.latitude)
        ?? 53.5439
        let long = Double(bankData.address.geoLocation.geographicCoordinates.longitude)
        ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat,
                                                longitude: long)
        let location = AnnotationsArray(type: type, title: title, locationName: locationName,
                                        coordinate: coordinate, idNumber: idNumber, address: address,
                                        workingHours: workingHours, issuedCurrency: issuedCurrency,
                                        cashIn: cashIn, city: city)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }

    func configure(bankData: Branch) {
        locationOfTheATMabel.text = Branch.getAddress(bankData: bankData)
        addressLineLabel.text = bankData.name
        infoboxIdLabel.text = "ID номер отделения банка: " + bankData.branchID
        workingHoursLabel.text = Branch.getworkingHoursBank(bankData: bankData)
        issuedCurrencyLabel.text = Branch.getIssuedCurrency(bankData: bankData)
        cbuLabel.text = bankData.cbu
        accountNumberLabel.text = Branch.getaAccountBumber(bankData: bankData)
        equeueAndWifiLabel.text = Branch.getaEqueueAndWifiLabel(bankData: bankData)
        segmentLabel.text = Branch.getSegment(bankData: bankData)
        additionalInformationLabel.text = Branch.getadditionalInformation(bankData: bankData)
        servicesLabel.text = Branch.getServices(bankData: bankData)
        informationLabel.text = Branch.getinformation(bankData: bankData)
        contactDetailsLabel.text = Branch.getCoordinate(bankData: bankData)
    }
}

extension FullBankInfoViewController {

    private func setupScrollView() {

        scrollView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(showRouteButton.snp.top).offset(-20)
        }

        contentView.snp.makeConstraints { make in
            make.centerX.equalTo(scrollView.snp.centerX)
            make.width.equalTo(scrollView.snp.width)
            make.top.equalTo(scrollView.snp.top)
            make.bottom.equalTo(scrollView.snp.bottom)
        }
    }

    private func setupButton() {
        showRouteButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(35)
            make.trailing.bottom.equalToSuperview().offset(-35)
            make.height.equalTo(35)
        }
    }

    private func addSubView() {
        view.addSubview(scrollView)
        view.addSubview(showRouteButton)
        scrollView.addSubview(contentView)
        contentView.addSubview(locationOfTheATMabel)
        contentView.addSubview(addressLineLabel)
        contentView.addSubview(infoboxIdLabel)
        contentView.addSubview(workingHoursLabel)
        contentView.addSubview(issuedCurrencyLabel)
        contentView.addSubview(cbuLabel)
        contentView.addSubview(accountNumberLabel)
        contentView.addSubview(        equeueAndWifiLabel)
        contentView.addSubview(        segmentLabel)
        contentView.addSubview(        additionalInformationLabel)
        contentView.addSubview(        servicesLabel)
        contentView.addSubview(        informationLabel)
        contentView.addSubview(contactDetailsLabel)
    }

    private func setupConstraints() {
        locationOfTheATMabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(contentView.snp.top).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        addressLineLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(locationOfTheATMabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        infoboxIdLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(addressLineLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        workingHoursLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(infoboxIdLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        issuedCurrencyLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(workingHoursLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        cbuLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(issuedCurrencyLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        accountNumberLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(cbuLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

                equeueAndWifiLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(accountNumberLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

                segmentLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(        equeueAndWifiLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

                additionalInformationLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(        segmentLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

                servicesLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(        additionalInformationLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

                informationLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(        servicesLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        contactDetailsLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(        informationLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
