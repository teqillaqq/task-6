//
//  FullInfoboxInfoViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 6.03.22.
//

import UIKit
import SnapKit
import MapKit

class FullInfoboxInfoViewController: UIViewController {

    var location: AnnotationsArray?
    var infoboxData: BelarusbankDataInfobox?
    private let multiplier: Double = (4/5)

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    private let contentView: UIView = {
        let contentView = UIView()
        return contentView
    }()

    private let showRouteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .darkGray
        button.setTitle(ContentConstants.showRouteTitle,
                        for: .normal)
        button.layer.cornerRadius = 18
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.white.cgColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(showRoute),
                         for: .touchUpInside)
        return button
    }()

    private let locationOfTheATMabel = UILabel.makeLabel()
    private let addressLineLabel = UILabel.makeLabel()
    private let infoboxIdLabel = UILabel.makeLabel()
    private let workingHoursLabel = UILabel.makeLabel()
    private let issuedCurrencyLabel = UILabel.makeLabel()
    private let infTypeLabel = UILabel.makeLabel()
    private let cashInLabel = UILabel.makeLabel()
    private let cashInInfoLabel = UILabel.makeLabel()
    private let infPrinterLabel = UILabel.makeLabel()
    private let getregionPlatejLabel = UILabel.makeLabel()
    private let popolneniePlatejLabel = UILabel.makeLabel()
    private let infStatusLabel = UILabel.makeLabel()
    private let contactDetailsLabel = UILabel.makeLabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubView()
        setupScrollView()
        setupConstraints()
        setupButton()
        guard let infoboxData = infoboxData else {
            return
        }
        configure(infoboxData: infoboxData)
    }

    @objc private func showRoute(location: AnnotationsArray) {
        guard let infoboxData = infoboxData else {
            return
        }
        let type = infoboxData.type
        let title = infoboxData.address
        let locationName = infoboxData.address
        let idNumber = String(infoboxData.infoID)
        let city = infoboxData.city
        let address = ""
        let workingHours = ""
        let issuedCurrency = ""
        let cashIn = ""
        let lat = Double(infoboxData.gpsX)
        ?? 53.5439
        let long = Double(infoboxData.gpsY)
        ?? 27.330
        let coordinate = CLLocationCoordinate2D(latitude: lat,
                                                longitude: long)
        let location = AnnotationsArray(type: type, title: title, locationName: locationName,
                                        coordinate: coordinate, idNumber: idNumber, address: address,
                                        workingHours: workingHours, issuedCurrency: issuedCurrency,
                                        cashIn: cashIn, city: city)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }

    func configure(infoboxData: BelarusbankDataInfobox) {
        locationOfTheATMabel.text = BelarusbankDataInfobox.getAddressgetCity(infobox: infoboxData)
        addressLineLabel.text = infoboxData.installPlace.rawValue + "\n" + infoboxData.locationNameDesc
        infoboxIdLabel.text = "ID номер инфокиоска: " + String(infoboxData.infoID)
        workingHoursLabel.text = BelarusbankDataInfobox.getworkingHoursFull(infobox: infoboxData)
        issuedCurrencyLabel.text = BelarusbankDataInfobox.getCurrencyl(infobox: infoboxData)
        infTypeLabel.text = BelarusbankDataInfobox.getinfType(infobox: infoboxData)
        cashInLabel.text = BelarusbankDataInfobox.getCashIn(infobox: infoboxData)
        cashInInfoLabel.text = BelarusbankDataInfobox.getcashInInfo(infobox: infoboxData)
        infPrinterLabel.text = BelarusbankDataInfobox.getinfPrinter(infobox: infoboxData)
        getregionPlatejLabel.text = BelarusbankDataInfobox.getregionPlatej(infobox: infoboxData)
        popolneniePlatejLabel.text = BelarusbankDataInfobox.getpopolneniePlatej(infobox: infoboxData)
        infStatusLabel.text = BelarusbankDataInfobox.getInfStatus(infobox: infoboxData)
        contactDetailsLabel.text = BelarusbankDataInfobox.getCoordinate(infobox: infoboxData)
    }
}

extension FullInfoboxInfoViewController {

    private func setupScrollView() {

        scrollView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(showRouteButton.snp.top).offset(-20)
        }

        contentView.snp.makeConstraints { make in
            make.centerX.equalTo(scrollView.snp.centerX)
            make.width.equalTo(scrollView.snp.width)
            make.top.equalTo(scrollView.snp.top)
            make.bottom.equalTo(scrollView.snp.bottom)
        }
    }

    private func setupButton() {
        showRouteButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(35)
            make.trailing.bottom.equalToSuperview().offset(-35)
            make.height.equalTo(35)
        }
    }

    private func addSubView() {
        view.addSubview(scrollView)
        view.addSubview(showRouteButton)
        scrollView.addSubview(contentView)
        contentView.addSubview(locationOfTheATMabel)
        contentView.addSubview(addressLineLabel)
        contentView.addSubview(infoboxIdLabel)
        contentView.addSubview(workingHoursLabel)
        contentView.addSubview(issuedCurrencyLabel)
        contentView.addSubview(infTypeLabel)
        contentView.addSubview(cashInLabel)
        contentView.addSubview(cashInInfoLabel)
        contentView.addSubview(infPrinterLabel)
        contentView.addSubview(getregionPlatejLabel)
        contentView.addSubview(popolneniePlatejLabel)
        contentView.addSubview(infStatusLabel)
        contentView.addSubview(contactDetailsLabel)
    }

    private func setupConstraints() {
        locationOfTheATMabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(contentView.snp.top).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        addressLineLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(locationOfTheATMabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        infoboxIdLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(addressLineLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        workingHoursLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(infoboxIdLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        issuedCurrencyLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(workingHoursLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        infTypeLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(issuedCurrencyLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        cashInLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(infTypeLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        cashInInfoLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(cashInLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        infPrinterLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(cashInInfoLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        getregionPlatejLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(infPrinterLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        popolneniePlatejLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(getregionPlatejLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        infStatusLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(popolneniePlatejLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
        }

        contactDetailsLabel.snp.makeConstraints { make in
            make.centerX.equalTo(contentView.snp.centerX)
            make.top.equalTo(infStatusLabel.snp.bottomMargin).offset(25)
            make.width.equalTo(contentView.snp.width).multipliedBy(multiplier)
            make.bottom.equalTo(contentView.snp.bottom)
        }
    }
}
