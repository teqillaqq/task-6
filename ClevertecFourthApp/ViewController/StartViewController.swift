//
//  StartViewController.swift
//  ClevertecFourthApp
//
//  Created by Александр Савков on 22.02.22.
//

import UIKit
import SnapKit
import MapKit
import CoreLocation
import Contacts

class StartViewController: UIViewController {

    private let scrollView = UIScrollView()
    private let contentView = UIView()
    private let activityIndicator = UIActivityIndicatorView()
    private let rangeInMeters: Double = 1700
    private var counter: Int = 1000
    private let mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    private var collectionView: UICollectionView?
    private lazy var atmData = [ATM]() {
        didSet {
            annotationsArray.removeAll { atmAnnotations in
                atmAnnotations.type == "ATM"
            }
            DispatchQueue.main.async {
                for item in self.atmData {
                    if let atm = AnnotationsArray.from(atmData: item) {
                        self.annotationsArray.append(atm)
                    }
                }
            }
        }
    }
    private lazy var infoboxData = [BelarusbankDataInfobox]() {
        didSet {
            annotationsArray.removeAll { atmAnnotations in
                atmAnnotations.type == "Infobox"
            }
            DispatchQueue.main.async {
                for item in self.infoboxData {
                    if let infobox = AnnotationsArray.from(infoboxData: item) {
                        self.annotationsArray.append(infobox)
                    }
                }
            }
        }
    }
    private lazy var bankData = [Branch]() {
        didSet {
            annotationsArray.removeAll { atmAnnotations in
                atmAnnotations.type == "Bank"
            }
            DispatchQueue.main.async {
                for item in self.bankData {
                    if let bank = AnnotationsArray.from(bankData: item) {
                        self.annotationsArray.append(bank)
                    }
                }
            }
        }
    }
    lazy var annotationsArray = [AnnotationsArray]()
    private lazy var atmDataByCity = Dictionary(grouping: annotationsArray,
                                                by: { $0.city }).sorted(by: { $0.key < $1.key })
    private lazy var objectArray = [GruppedByCytyObject]()
    private var previousLocation: CLLocation?
    lazy var locationManager: CLLocationManager = {
        var manager = CLLocationManager()
        manager.distanceFilter = 10
        manager.desiredAccuracy = kCLLocationAccuracyBest
        return manager
    }()

    private lazy var segmentedControl: UISegmentedControl = {
        segmentedControl = UISegmentedControl(items: [ContentConstants.segmentedControlFirstItem,
                                                      ContentConstants.segmentedControlSecondItem])
        segmentedControl.sizeToFit()
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(switchViewController(_ :)),
                                   for: .valueChanged)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationManager.delegate = self
        mapView.register(BelarusBankMarkerView.self, forAnnotationViewWithReuseIdentifier:
                            MKMapViewDefaultAnnotationViewReuseIdentifier)
        locationManager.requestWhenInUseAuthorization()
        initialize()
        fetchData()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: ContentConstants.reload,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(reloadData))
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: ContentConstants.filter,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(filterData))
        navigationItem.leftBarButtonItem?.isEnabled = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationServices()
    }

    private func fetchData() {
        activityIndicator.startAnimating()
        let myQueue = DispatchQueue(label: "com.myApp.myQueue", qos: .background,  attributes: [.concurrent])
        let dispatchGroup = DispatchGroup()

        myQueue.async(group: dispatchGroup) {
            dispatchGroup.enter()
            DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {

                NetworkManager.shared.fetchAtms { result in
                    switch result {
                    case .success(let ATMs):
                        self.atmData = ATMs.data.atm
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.alertError(with: error, titleOfData: ContentConstants.atmTitle)
                        }
                    }
                    dispatchGroup.leave()
                }
            }
        }

        myQueue.async(group: dispatchGroup) {
            dispatchGroup.enter()
            DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {

                NetworkManager.shared.fetchInfobox { result in
                    switch result {
                    case .success(let Infobox):
                        self.infoboxData = Infobox
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.alertError(with: error, titleOfData: ContentConstants.infoboxTitle)
                        }
                    }
                    dispatchGroup.leave()
                }
            }
        }

        myQueue.async(group: dispatchGroup) {
            dispatchGroup.enter()
            DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) {

                NetworkManager.shared.fetchBank { result in
                    switch result {
                    case .success(let bank):
                        self.bankData = bank.data.branch
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.alertError(with: error, titleOfData: ContentConstants.bankTitle)
                        }
                    }
                    dispatchGroup.leave()
                }
            }
        }
        myQueue.async {
            dispatchGroup.wait()
        }
        dispatchGroup.notify(queue: DispatchQueue.main) {
            self.addAnatations()
            self.activityIndicator.stopAnimating()
            self.centerViewOnUser()
        }
    }

    private func addAnatations() {
        mapView.addAnnotations(annotationsArray)
        navigationItem.rightBarButtonItem?.isEnabled = true
        navigationItem.leftBarButtonItem?.isEnabled = true
    }

    @objc private func reloadData() {
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.leftBarButtonItem?.isEnabled = false
        fetchData()
    }

    @objc private func filterData() {
        var popUpWindow: FilterPopUpView
        popUpWindow = FilterPopUpView(title: "Выберите тип объектов")
        NotificationCenter.default.addObserver(self, selector: #selector(action(_:)),
                                               name: NSNotification.Name(rawValue: "int"),
                                               object: nil)
        self.present(popUpWindow, animated: true, completion: nil)
    }

    @objc private func action(_ notification: NSNotification) {
           if let dict = notification.userInfo as NSDictionary? {
                       if let id = dict["int"] as? Int {
                           self.counter = id
                       }
                   }
            }

    @objc private func switchViewController(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            mapView.isHidden = false
            guard let collectionView = collectionView else {
                return
            }
            collectionView.isHidden = true
        case 1:
            mapView.isHidden = true
            for (key, value) in atmDataByCity {
                objectArray.append(GruppedByCytyObject(sectionName: key,
                                                       sectionObjects: value.sorted(by: { first, second in
                    return first.idNumber < second.idNumber
                })))
            }
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }
            createCollection()
        default:
            print(ContentConstants.errorSwitchVontroller)
        }
    }

    private func filterMap(counter: Int) {
        switch counter {
        case 0:
            annotationsArray.removeAll()
        case 1:
            annotationsArray.removeAll { annotation in
                annotation.type == "Infobox"
            }
            annotationsArray.removeAll { annotation in
                annotation.type == "Bank"
            }
        case 11:
            annotationsArray.removeAll { annotation in
                annotation.type == "Bank"
            }
        case 111:
            print("")
        case 101:
            annotationsArray.removeAll { annotation in
                annotation.type == "Infobox"
            }
        case 10:
            annotationsArray.removeAll { annotation in
                annotation.type == "ATM"
            }
            annotationsArray.removeAll { annotation in
                annotation.type == "Bank"
            }
        case 100:
            annotationsArray.removeAll { annotation in
                annotation.type == "ATM"
            }
            annotationsArray.removeAll { annotation in
                annotation.type == "Infobox"
            }
        case 110:
            annotationsArray.removeAll { annotation in
                annotation.type == "ATM"
            }
        default:
            print("")
        }
        mapView.showAnnotations(annotationsArray, animated: true)
    }

    private func createCollection() {
        let layout = UICollectionViewFlowLayout ()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 50, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: (mapView.bounds.size.width * 0.3) - 4,
                                 height: (mapView.bounds.size.width * 0.65) - 4)
        layout.headerReferenceSize = CGSize(width: 200, height: 75)
        collectionView = UICollectionView(frame: mapView.frame, collectionViewLayout: layout)
        guard let collectionView = collectionView else { return }
        collectionView.register(CollectionViewCell.self,
                                forCellWithReuseIdentifier: CollectionViewCell.identifier)
        collectionView.register(SectionHeader.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: SectionHeader.reuserID)
        collectionView.dataSource = self
        collectionView.delegate = self
        view.addSubview(collectionView)
        collectionView.frame = mapView.frame
        collectionView.backgroundColor = ColorConstants.background
    }

    // MARK: - Location Services

    private func checkLocationServices() {
        guard CLLocationManager.locationServicesEnabled() else {
            showSettingsAlert()
            return
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        checkAuthorizationForLocation()
    }

    private func checkAuthorizationForLocation() {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse, .authorizedAlways:
            mapView.showsUserLocation = true
            centerViewOnUser()
            locationManager.startUpdatingLocation()
            previousLocation = getCenterLocation(for: mapView)
            break
        case .denied:
            showSettingsAlert()
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            showSettingsAlert()
            break
        @unknown default:
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            break
        }
    }

    private func centerViewOnUser() {
        guard let location = locationManager.location?.coordinate else { return }
        let coordinateRegion = MKCoordinateRegion.init(center: location,
                                                       latitudinalMeters: rangeInMeters,
                                                       longitudinalMeters: rangeInMeters)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        let coordinates = mapView.centerCoordinate
        return CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
    }
}

extension StartViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {

        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
}

// MARK: - MKMapViewDelegate

extension StartViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        var popUpWindow: PopUpWindow
        guard let annotation = view.annotation as? AnnotationsArray else { return }

        if let selectedAtm = atmData.first(where: { atm in
            atm.atmID == annotation.locationName
        }) {
            popUpWindow = PopUpWindow(title: annotation.title ?? annotation.type, atmAnnotations: annotation)
            popUpWindow.atmData = selectedAtm
            popUpWindow.selectedAnatation = annotation
            self.present(popUpWindow, animated: true, completion: nil)
        } else
        if let selectedInfobox = infoboxData.first(where: { infobox in
            String(infobox.infoID) == annotation.locationName
        }) {
            popUpWindow = PopUpWindow(title: annotation.title ?? annotation.type, atmAnnotations: annotation)
            popUpWindow.infoboxData = selectedInfobox
            popUpWindow.selectedAnatation = annotation
            self.present(popUpWindow, animated: true, completion: nil)
        } else
        if let selectedBank = bankData.first(where: { bank in
            bank.branchID == annotation.locationName
        }) {
            popUpWindow = PopUpWindow(title: annotation.title ?? annotation.type, atmAnnotations: annotation)
            popUpWindow.bankData = selectedBank
            popUpWindow.selectedAnatation = annotation
            self.present(popUpWindow, animated: true, completion: nil)
        }
    }
}

// MARK: - Constraints for segmentedControl, mapView

extension StartViewController {

    private func initialize() {
        title = ContentConstants.startVCtitle
        view.backgroundColor = ColorConstants.background

        view.addSubview(segmentedControl)
        segmentedControl.snp.makeConstraints { make in
            make.top.left.right.equalTo(view.safeAreaLayoutGuide)
            make.height.lessThanOrEqualTo(35)
        }

        view.addSubview(mapView)
        mapView.snp.makeConstraints { make in
            make.top.equalTo(segmentedControl.snp.bottom)
            make.left.right.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(view)
        }

        mapView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        }
    }
}

// MARK: - CollectionView DataSource, Delegate

extension StartViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return objectArray.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objectArray[section].sectionObjects.count
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if let header = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: SectionHeader.reuserID,
            for: indexPath) as? SectionHeader {
            header.titleForHeader.text = objectArray[indexPath.section].sectionName
            return header
        }
        let header = SectionHeader()
        return header
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier,
                                                         for: indexPath) as? CollectionViewCell {
            cell.configure(atmAnnotations: objectArray[indexPath.section].sectionObjects[indexPath.row])
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier,
                                                      for: indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var popUpWindow: PopUpWindow

        if let selectedAtm = atmData.first(where: { atm in
            atm.atmID == objectArray[indexPath.section].sectionObjects[indexPath.row].idNumber
        }) {
            popUpWindow = PopUpWindow(title: objectArray[indexPath.section].sectionObjects[indexPath.row].title ??
                                      objectArray[indexPath.section].sectionObjects[indexPath.row].type,
                                      atmAnnotations: objectArray[indexPath.section].sectionObjects[indexPath.row])
            popUpWindow.atmData = selectedAtm
            popUpWindow.selectedAnatation = objectArray[indexPath.section].sectionObjects[indexPath.row]
            self.present(popUpWindow, animated: true, completion: nil)
        } else

        if let selectedInfobox = infoboxData.first(where: { infobox in
            String(infobox.infoID) == objectArray[indexPath.section].sectionObjects[indexPath.row].idNumber
        }) {
            popUpWindow = PopUpWindow(title: objectArray[indexPath.section].sectionObjects[indexPath.row].title ??
                                      objectArray[indexPath.section].sectionObjects[indexPath.row].type,
                                      atmAnnotations: objectArray[indexPath.section].sectionObjects[indexPath.row])
            popUpWindow.infoboxData = selectedInfobox
            popUpWindow.selectedAnatation = objectArray[indexPath.section].sectionObjects[indexPath.row]
            self.present(popUpWindow, animated: true, completion: nil)
        } else
        if let selectedBank = bankData.first(where: { bank in
            bank.branchID == objectArray[indexPath.section].sectionObjects[indexPath.row].idNumber
        }) {
            popUpWindow = PopUpWindow(title: objectArray[indexPath.section].sectionObjects[indexPath.row].title ??
                                      objectArray[indexPath.section].sectionObjects[indexPath.row].type,
                                      atmAnnotations: objectArray[indexPath.section].sectionObjects[indexPath.row])
            popUpWindow.bankData = selectedBank
            popUpWindow.selectedAnatation = objectArray[indexPath.section].sectionObjects[indexPath.row]
            self.present(popUpWindow, animated: true, completion: nil)
        }
        segmentedControl.selectedSegmentIndex = 0
        mapView.isHidden = false
        collectionView.isHidden = true

    }
}
